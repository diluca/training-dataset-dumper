Flavor Tagging Ntuple Dumper
============================

This is to dump b-tagging info from an AnalysisBase release

To compile the code, go to your (clean) project directory and run:

```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
source training-dataset-dumper/setup-analysisbase.sh
mkdir build
cd build
cmake ../training-dataset-dumper
make
```

As next step the following file needs to be sourced in order to add the executables to the system path

```
source build/x*/setup.sh
```

Then to run a test

```bash
test-dumper pflow
```

This will run the program in

```text
BTagTrainingPreprocessing/utils/dump-single-btag.cxx
```

which will dump some xAOD information to HDF5.

### Restoring setup

The next time you want to use the utility run from the project directory

```
source training-dataset-dumper/setup-analysisbase.sh
source build/x*/setup.sh
```

### Using Docker Container

Alternatively there is a Docker image available to use straight away.

The Master branch is built with the `latest` tag
```
gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper:latest
```
You can use the image e.g. with sinfularity via
```
singularity --silent run docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper:latest
```
with the `-B` option you can mount different paths into your image.


Running on the grid
-------------------

From the directory where you checked out the package, after running
the `setup.sh` script above, run the following:

```bash
source training-dataset-dumper/grid/setup.sh
```

Then run

```bash
./training-dataset-dumper/grid/submit.sh
```

This should submit a grid test job. Note that it's still a work in
progress, take a look at the script to see how it works.


Running with Athena
-------------------

For more complicated cases (e.g. retagging jets) we also provide an
Athena algorithm, `SingleBTagAlg`, (technically a duel-use
`EL::AnaAlgorithm`) which wraps the same functionality as the stand
alone executable.

To use this you'll have to use `athena-setup.sh` in place of
`setup.sh`, and rebuild the package against the Athena release. A very
simple `ComponentAccumulator`-based script to run it is provided in
`ca-dump-single-btag`.


Package Layout
--------------

The code lives under `BTagTrainingPreprocessing`. All the top-level
executables live in `util/`, whereas various private internal classes
are defined in `src/`.


Adding more variables
---------------------

Adding a new variable to the output files should be two simple steps:

 - Add a decoration to the `BTagging` object which you can access via
   `Jet::btagging()`.
 - Add this variable to the outputs in your configuration file.

There's an example class in `src/DecoratorExample.cxx` which should
make the first step a bit more clear. This is also instanced in
`util/dump-single-btag.cxx`.

The configuration json file should have an object called
`"variables"`, which specifies the outputs. These are also specified
by type: there is one list for `"floats"`, one for `"chars"`,
etc. There are also `"jet_int_variables"` and `"jet_floats"` for
variables that are stored on the jet itself (not the `BTagging`
object).

You should add whatever you've decorated to the b-tagging object to
the output list. If the output isn't found the code will fail loudly.


Configuration files
-------------------

Configuration files live under `training-dataset-dumper/configs`.
They are generally flat json files, with one special key: `file`.
If this appears in a json object, the parser will assume the key
gives a path _relative to the current file_. Any entries in the file
will be imported at the same scope where the `file` key appears. If
local keys conflict with imported ones, the local keys will be used.

### Other configuration notes

 - The `n_jets_per_event` option will change the output format.
   If this is set to 0 we store one jet per entry. If it's set to
   a value larger than 0 each entry will be `n` jets, and a separate
   object with an `_event` suffix will store event information.

Other Tricks
------------

You can examine HDF5 files with `h5ls`. See `h5ls -h` for more
information. I also wrote a nice [tab-complete script for][1] to make
this even better. Download it and then source it in your `.bashrc`.

[1]: https://github.com/dguest/_h5ls
