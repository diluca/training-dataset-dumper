#ifndef TRUTH_TOOLS_HH
#define TRUTH_TOOLS_HH

#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthParticleContainer.h"


namespace truth {
  class TruthRecordError: public std::runtime_error
  {
    using std::runtime_error::runtime_error;
  };
  bool is_from_WZ(const xAOD::TruthParticle& truth_particle);
  bool is_overlaping_lepton(
    const xAOD::Jet& jet,
    const std::vector<const xAOD::TruthParticle*>& truth_particles, float dR);
  std::vector<const xAOD::TruthParticle*> getLeptonsFromWZ(
    const std::vector<const xAOD::TruthParticle*>&);
}

#endif
