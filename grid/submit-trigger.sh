#!/usr/bin/env bash

# This script should not be sourced, we don't need anything in here to
# propigate to the surrounding environment.
if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
  # set the shell to exit if there's an error (-e), and to error if
  # there's an unset variable (-u)
    set -eu
fi


##########################
# Real things start here #
##########################
#
# part 0 is parsing arguments
#
function _help() {
    cat <<EOF
usage: ${0##*/} [-h] [options] CONFIG_FILE

Submit a job to the grid! The CONFIG_FILE is required, and should live
under /configs in this package. Note that this script will ask you to
commit your changes, but it's a good idea to tag them as well.

Options:
 -f: force, don't require changes to be committed

EOF
}

FORCE=''
while getopts ":hf" opt $@;
do
    case $opt in
        h) _help; exit 1;;
        f) FORCE=1 ;;
    esac
done
shift $((OPTIND-1))


###################################################
# Part 1: variables you you _might_ need to change
###################################################
#
# Users's grid name
GRID_NAME=${RUCIO_ACCOUNT-${USER}}
#
DEFAULT_CONFIG=Trigger.json

# R22 validation samples from ATR-23543
INPUT_DATASETS=(
    valid1.364703.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3WithSW.recon.AOD.e7142_s3681_d1660_r12684
    valid1.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e4993_s3214_d1656_r12684
    valid1.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.recon.AOD.e5362_s3459_d1656_r12684
)

######################################################
# Part 2: variables you probably don't have to change
######################################################
#
# Build a zip of the files we're going to submit
ZIP=job.tgz
#
# This is the subdirectory we submit from
SUBMIT_DIR=submit
#
# This is where all the source files are
BASE=$(cd ${BASH_SOURCE[0]%/*}/..; pwd)
#
# Configuration file stuff
DEFAULT_CONFIG_PATH=${BASE}/configs/single-b-tag/${DEFAULT_CONFIG}
#
# The executable
EXE=ca-dump-trigger-btag
#
# Check that we don't have uncontrolled changes
GIT_TAG=$(
    cd ${BASH_SOURCE[0]%/*}
    if ! git diff-index --quiet HEAD; then
        if [[ $FORCE ]]; then
            date +%F-T%H%M%S
            exit 0
        fi
        echo "ERROR: uncommitted changes, please commit them" >&2
        exit 1
    fi
    git describe
)

###################################################
# Part 3: prep the submit area
###################################################
#
echo "preping submit area"
if [[ -d ${SUBMIT_DIR} ]]; then
    echo "removing old submit directory"
    rm -rf ${SUBMIT_DIR}
fi
mkdir ${SUBMIT_DIR}
CONFIG_PATH=${1-${DEFAULT_CONFIG_PATH}}
echo "using config file ${CONFIG_PATH}"
cp ${CONFIG_PATH} ${SUBMIT_DIR}
# make sure we send files that the configuration depends on too
cp -r ${DEFAULT_CONFIG_PATH%/*}/fragments ${SUBMIT_DIR}
cd ${SUBMIT_DIR}


##########################################
# Part 4: build a tarball of the job
###########################################
#
# Check to make sure you've properly set up the environemnt: if you
# haven't sourced the setup script in the build directory the grid
# submission will fail, so we check here before doing any work.
if ! type $EXE &> /dev/null ; then
    echo "You haven't sourced x86*/setup.sh, job will fail!" >&2
    echo "quitting..." >&2
    exit 1
fi
#
echo "making tarball of local files: ${ZIP}" >&2
#
# The --outTarBall, --noSubmit, and --useAthenaPackages arguments are
# important. The --outDS and --exec don't matter at all here, they are
# just placeholders to keep panda from complianing.
prun --outTarBall=${ZIP} --noSubmit --useAthenaPackages\
     --exec "ls"\
     --outDS user.${GRID_NAME}.x

##########################################
# Part 5: loop over datasets and submit
##########################################

# Loop over all inputs
echo "submitting for ${#INPUT_DATASETS[*]} datasets"
#
for DS in ${INPUT_DATASETS[*]}
do
   # This regex extracts the DSID from the input dataset name, so
   # that we can give the output dataset a unique name. It's not
   # pretty: ideally we'd just suffix our input dataset name with
   # another tag. But thanks to insanely long job options names we
   # use in the generation stage we're running out of space for
   # everything else.
   DSID=$(sed -r 's/[^\.]*\.([0-9]{6,8})\..*/\1/' <<< ${DS})
   #
   # Build the full output dataset name
   CONFIG_FILE=${CONFIG_PATH##*/}
   TAGS=$(cut -d . -f 6 <<< ${DS}).${CONFIG_FILE%.*}.${AtlasBuildStamp}
   OUT_DS=user.${GRID_NAME}.${DSID}.trig.${TAGS}.${GIT_TAG}
   #
   # Now submit. The script we're running expects one argument per
   # input dataset, whereas %IN gives us comma separated files, so we
   # have to run it through `tr`.
   #
   echo "Submitting for ${GRID_NAME} on ${DS} -> ${OUT_DS}"
   prun --exec "${EXE} %IN -c ${CONFIG_FILE}"\
        --outDS ${OUT_DS} --inDS ${DS}\
        --useAthenaPackages --inTarBall=${ZIP}\
        --nFilesPerJob 1\
        --mergeScript="hdf5-merge-nolock -o %OUT -i %IN"\
        --outputs output.h5\
        --noEmail > ${OUT_DS}.log 2>&1 &
   sleep 1

done
wait
