#ifndef SINGLEBTAG_CONFIG_HH
#define SINGLEBTAG_CONFIG_HH

#include "DL2Config.hh"

#include <string>
#include <vector>
#include <map>
#include "TrackSelectorConfig.hh"
#include "TrackSortOrder.hh"

typedef std::map<std::string,std::vector<std::string>> VariableList;

struct TrackConfig {
  size_t n_to_save;
  TrackSortOrder sort_order;
  TrackSource source;
  TrackSelectorConfig selection;
  VariableList variables;
  std::string input_name;
  std::string output_name;
};

struct SingleBTagConfig {
  std::string jet_collection;
  std::string jet_calibration_collection;
  std::string jet_calib_file;
  std::string cal_seq;
  std::string cal_area;
  bool do_calibration;
  bool run_augmenters;
  float jvt_cut;
  float pt_cut;
  std::string vertex_collection;
  std::vector<DL2Config> dl2_configs;
  VariableList btag;
  std::vector<TrackConfig> tracks;
  size_t n_jets_per_event;
};

SingleBTagConfig get_singlebtag_config(const std::string& config_file_name);


#endif
