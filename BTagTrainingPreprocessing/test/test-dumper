#!/usr/bin/env bash

set -Eeu

print-usage() {
    echo "usage: $0 [-h] [-d <dir>] (pflow|eventloop|hbb)" 1>&2
}

usage() {
    print-usage
    exit 1;
}

help() {
    print-usage
    cat <<EOF

The ${0##*/} utility will download a test DAOD file and use it to
produce a training dataset.

Options:
 -d: specify directory to run in
 -h: print help

If no -d argument is given we'll create one in /tmp and work there.

EOF
    exit 1
}

DIRECTORY=""
DATA_URL=https://dguest-ci.web.cern.ch/dguest-ci/ci/ftag/dumper

while getopts ":d:h" o; do
    case "${o}" in
        d) DIRECTORY=${OPTARG} ;;
        h) help ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))

if (( $# != 1 )) ; then
    usage
fi

MODE=$1

################################################
# mode-based switches
################################################

# config mapping
#
CFG_DIR=$(dirname $(readlink -e ${BASH_SOURCE[0]}))/../../configs
declare -A CONFIGS=(
    [pflow]=${CFG_DIR}/single-b-tag/EMPFlow.json
    [eventloop]=${CFG_DIR}/single-b-tag/EMPFlow.json
    [hbb]=${CFG_DIR}/hbb/2020_ftag5dev.json
)
declare -A DATAFILES=(
    [pflow]=22.2.X/2021-05-09T2101/DAOD_PHYSVAL.small.pool.root
    [eventloop]=22.2.X/2021-05-09T2101/DAOD_PHYSVAL.small.pool.root
    [hbb]=22.2.X/2021-05-09T2101/DAOD_PHYSVAL.small.pool.root
)
declare -A TESTS=(
    [pflow]=single-b
    [eventloop]=eventloop
    [hbb]=hbb
)


# if we're using AthAnalysis (or athena) we don't use eventloop
if command -v ca-dump-single-btag > /dev/null
then
    EVENTLOOP_COMMAND=ca-dump-single-btag
else
    EVENTLOOP_COMMAND=el-dump-single-btag
fi


# define variables used later on
if [[ ! ${CONFIGS[$MODE]+x} ]]; then usage; fi
CFG=${CONFIGS[$MODE]}

if [[ ! ${DATAFILES[$MODE]+x} ]]; then usage; fi
DOWNLOAD_PATH=${DATAFILES[$MODE]}
FILE=${DOWNLOAD_PATH##*/}


# run scripts based on mode
function single-b {
    local CMD="dump-single-btag $FILE -c $CFG"
    echo "running ${CMD}"
    ${CMD}
}
function eventloop {
    local CMD="${EVENTLOOP_COMMAND} $FILE -c $CFG"
    echo "running ${CMD}"
    ${CMD}
}
function hbb {
    local CMD="dump-hbb $FILE -c $CFG"
    echo "running ${CMD}"
    ${CMD}
}
if [[ ! ${TESTS[$MODE]+x} ]]; then usage; fi
RUN=${TESTS[$MODE]}


#############################################
# now start doing stuff
#############################################
#
if [[ -z ${DIRECTORY} ]] ; then
    DIRECTORY=$(mktemp -d)
    echo "running in ${DIRECTORY}" >&2
fi

if [[ ! -d ${DIRECTORY} ]]; then
    if [[ -e ${DIRECTORY} ]] ; then
        echo "${DIRECTORY} is not a directory" >&2
        exit 1
    fi
    mkdir ${DIRECTORY}
fi
cd $DIRECTORY

# get files
if [[ ! -f ${FILE} ]] ; then
    echo "getting file ${FILE}" >&2
    curl -s ${DATA_URL}/${DOWNLOAD_PATH} > ${FILE}
fi

${RUN}




