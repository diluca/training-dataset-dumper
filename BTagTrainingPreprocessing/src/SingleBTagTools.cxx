#include "SingleBTagTools.hh"

#include "SingleBTagConfig.hh"
#include "BTaggingWriterConfiguration.hh"
#include "BTagJetWriterConfig.hh"
#include "BTagTrackWriterConfig.hh"

#include "H5Cpp.h"

namespace {
  std::vector<std::string> get(const VariableList& v, const std::string& k) {
    if (v.count(k)) return v.at(k);
    return {};
  }
  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }


  BTagJetWriterConfig jwConfig(const SingleBTagConfig& jobcfg) {
    BTagJetWriterConfig jet_cfg;
    jet_cfg.event_info = get(jobcfg.btag, "event");
    jet_cfg.char_variables = get(jobcfg.btag, "chars");
    jet_cfg.jet_int_variables = get(jobcfg.btag, "jet_int_variables");
    jet_cfg.jet_float_variables = get(jobcfg.btag,"jet_floats");
    jet_cfg.int_as_float_variables = get(jobcfg.btag, "ints_as_float");
    jet_cfg.float_variables = get(jobcfg.btag, "floats");
    jet_cfg.double_variables = get(jobcfg.btag, "doubles");
    jet_cfg.variable_maps.replace_with_defaults_checks = cfg::check_map_from(cfg::BTagDefaultsMap);
    jet_cfg.variable_maps.rename = {}; // please don't use this :(
    jet_cfg.n_jets_per_event = jobcfg.n_jets_per_event;
    jet_cfg.name = "jets";
    return jet_cfg;
  }
  BTagTrackWriterConfig twConfig(const TrackConfig& jobcfg) {
    BTagTrackWriterConfig track_cfg;
    track_cfg.name = jobcfg.output_name;
    track_cfg.uchar_variables = get(jobcfg.variables, "uchar");
    track_cfg.int_variables = get(jobcfg.variables, "ints");
    track_cfg.float_variables = get(jobcfg.variables, "floats");
    track_cfg.output_size = {jobcfg.n_to_save};
    track_cfg.flavortagdiscriminants_sequences = get(
      jobcfg.variables, "flavortagdiscriminants_sequences");
    return track_cfg;
  }
}

TrackTools::TrackTools(const TrackConfig& cfg, H5::H5File& output):
  selector(cfg.selection, cfg.input_name, cfg.source),
  sort(trackSort(cfg.sort_order)),
  writer(output, twConfig(cfg))
{
}
TrackTools::TrackTools(TrackTools&&) = default;

SingleBTagTools::SingleBTagTools(const SingleBTagConfig& jobcfg,
                                 H5::H5File& output):
  calibration_tool("JetCalibrationTool"),
  cleaning_tool("JetCleaningTool", JetCleaningTool::LooseBad, false),
#ifndef DISABLE_JVT
  jvttool("JetVertexTaggerTool"),
#endif
  muon_augmenter("Muons"),
  jet_writer(output, jwConfig(jobcfg))
{
  if (jobcfg.do_calibration){
    JetCalibrationTool& jct = calibration_tool;
    check_rc( jct.setProperty("JetCollection",
                              jobcfg.jet_calibration_collection) );
    check_rc( jct.setProperty("ConfigFile", jobcfg.jet_calib_file) );
    check_rc( jct.setProperty("CalibSequence", jobcfg.cal_seq) );
    check_rc( jct.setProperty("CalibArea", jobcfg.cal_area) );
    check_rc( jct.setProperty("IsData", false) );
    check_rc( jct.initialize() );
  }
  check_rc( cleaning_tool.initialize() );

  check_rc( jvttool.setProperty("JetContainer", jobcfg.jet_collection));
  check_rc( jvttool.initialize() );

  for (const auto& cfg: jobcfg.dl2_configs) {
    using FlavorTagDiscriminants::FlipTagConfig;
    std::string path = cfg.nn_file_path;
    std::cout << "loading " << path << std::endl;
    dl2s.emplace_back(path, FlipTagConfig::STANDARD, cfg.output_remapping);
  }
  for (const TrackConfig& cfg: jobcfg.tracks) {
    tracks.emplace_back(cfg, output);
  }

}
