#include <AnaAlgorithm/AnaAlgorithm.h>

#include "SingleBTagConfig.hh"

class SingleBTagTools;
class SingleBTagConfig;
namespace H5 {
  class H5File;
}

class SingleBTagAlg: public EL::AnaAlgorithm
{
public:
  SingleBTagAlg(const std::string& name, ISvcLocator* pSvcLocator);
  ~SingleBTagAlg();

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
private:
  std::string m_output_file;
  std::string m_config_file_name;

  std::unique_ptr<SingleBTagConfig> m_config;

  std::unique_ptr<H5::H5File> m_output;
  std::unique_ptr<SingleBTagTools> m_tools;
};
